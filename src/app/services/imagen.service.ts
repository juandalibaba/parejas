import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImagenService {
  imagenes = [
    "https://image.freepik.com/free-vector/vector-illustration-cosmonaut_1441-11.jpg",
    "https://image.freepik.com/free-vector/polygonal-lion-head_23-2147495868.jpg",
    "https://image.freepik.com/free-vector/hand-painted-steampunk-man-illustration_23-2147537528.jpg",
    "https://image.freepik.com/free-vector/analytical-and-creative-brain_23-2147506845.jpg",
    "https://image.freepik.com/free-vector/abstract-floral-background_1005-10.jpg",
    "https://image.freepik.com/free-vector/thank-you-composition-in-comic-style_23-2147831785.jpg",
    "https://media.giphy.com/media/3ornk2zKMUgETO4aMo/giphy.gif"
  ]

  imagenBack = "https://image.freepik.com/foto-gratis/hermoso-fondo-de-cobre-oxidado-de-cardenillo_24837-103.jpg"

  constructor() { }

  // devuelve la url de la imagen `n`
  get(n: number) {
    let i = (n > this.imagenes.length - 1) ? this.imagenes.length - 1 : n
    return this.imagenes[i]
  }

  // devuelve el nº total de imágenes
  getNumberOfImages() {
    return this.imagenes.length
  }

  // devuelve la imagen que se utilizará como envés de la carta
  getBackImage() {
    return this.imagenBack
  }

  // Un número aleatorio entre `min` y `max`
  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  // Una distribución aleatoria de pares de números
  getRandomDistribution(): number[] {
    let orderedList = []
    for (let i = 0; i < this.imagenes.length; i++) {
      orderedList.push(i)
      orderedList.push(i)
    }
    console.log(orderedList)
    let unorderedList = []
    let iter = 2 * this.imagenes.length
    let _iter = iter
    for (let i = 0; i < iter; i++) {
      let index = this.randomIntFromInterval(0, _iter - 1)
      unorderedList.push(orderedList[index])
      _iter --
      orderedList.splice(index, 1);
    }
    console.log(unorderedList)
    return unorderedList
  }
}
