import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { ImagenService } from '../services/imagen.service';
import { globalState } from '../state';

@Component({
  selector: 'app-carta',
  templateUrl: './carta.component.html',
  styleUrls: ['./carta.component.css']
})
export class CartaComponent implements OnInit, OnChanges {

  // El nº de imagen que presenta el componente
  @Input('n') n: number
  // El estado global, un array con no más de 2 elementos que 
  // representa las imágenes que se han descubierto 
  @Input('globalState') globalState: number[]
  // Un emisor de eventos que notifica al componente padre cuando
  // se descubre la imagen asociada al componente. Simplemente envía
  // el nº de imagen, es decir `this.n`
  @Output('onChangeState') onChangeState = new EventEmitter<number>()
  // La imagen que se debe mostrar: la que corresponde al índice `n`
  // o la que corresponde a la parte oculta (la parte de atrás de la carta)
  urlImage: string
  // La imagen de atrás, común a todas las "cartas"
  urlImageBack: string

  // Flag que indica si esta carta ya está resuelta, es decir, se ha 
  // encontrado su pareja.
  resolved = false

  constructor(private imagenService: ImagenService) {
    this.urlImageBack = imagenService.getBackImage()
  }

  ngOnInit() {
    // Cada carta comienza mostrando la imagen de atrás común a todas
    this.urlImage = this.urlImageBack
  }

  ngOnChanges(changes: SimpleChanges) {
    // Aquí detectamos los cambios en el estado global que es un atributo
    // que se pilla como entrada (@Input) desde el componente padre, y 
    // que va cambiando conforme el juego se desarrolla.
    const globalState: number[] = changes.globalState.currentValue
    // Aquí la lógica para dar la vuelta a la carta
    if (globalState.length == 2
      && globalState[0] != globalState[1]
      && !this.resolved) {
      setTimeout(() => { this.urlImage = this.urlImageBack }, 1000)
    }
    // Aquí la lógica para dejar la carta boca arriba (resuelta)
    if (globalState.length == 2
      && globalState[0] == globalState[1]
      && globalState[0] == this.n) {
      this.resolved = true
    }
  }

  flip() {
    // Cuando el usuario pica en la carta hay que darle la vuelta, es decir,
    // mostrar la imagen que le corresponde al índice `n`
    this.urlImage = this.imagenService.get(this.n)
    // y se notifica al componente padre para que actualice el estado global.
    this.onChangeState.emit(this.n)
  }
}
