import { Component } from '@angular/core';
import { ImagenService } from './services/imagen.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // El estado global que consiste en un array de números. 
  // Cada número del array representa a una imagen descubierta.
  // Por ello no puede haber más de 2 elementos en el array, es decir,
  // puede haber ninguno, uno o dos y va evolucionando en ese orden a
  // medida que se va jugando.
  gs: number[] = []

  // La distribución aleatoria de cartas que conforman el juego
  cardDistribution: number[]

  constructor(private imagenService: ImagenService){
    // console.log(imagenService.getRandomDistribution())
  }

  ngOnInit(){
    this.cardDistribution = this.imagenService.getRandomDistribution()
  }

  changeGlobalState(event) {
    console.log("Se ha levantado la carta: " + event)
    console.log("estado antes: [" + this.gs.toString() + ']')
    // Si hay dos elementos en el array, hay que reiniciarlo, sean o no
    // iguales. Que las imágenes queden descubiertas o no es algo que 
    // decidirá el componente imagen.
    if (this.gs.length == 2) {
      this.gs = []
    } else {
      // Truco del almendruco. Debido a que angular en el evento onChanges
      // no detecta cambios que se produzcan en un array (añadir o quitar elementos),
      // hay que reasignar el array completo.
      // Por eso se crea una copia temporal del array de estados y se
      // reasigna. 
      let _gs = JSON.parse(JSON.stringify(this.gs))
      this.gs = _gs
    }
    // Se añade el estado que llega desde el componente
    this.gs.push(event)
    console.log("estado después: [" + this.gs.toString() + ']')
  }
}
